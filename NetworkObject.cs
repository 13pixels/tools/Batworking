﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.IO;

    /// <summary>
    /// Add this component to a prefab for sending messages between the server and clients' representations of a GameObject.
    /// </summary>
    public sealed class NetworkObject : MonoBehaviour
    {
        #region Payload Structs
        public struct Payload<T0, T1>
        {
            public T0 p0;
            public T1 p1;

            public void WriteToArray(object[] target)
            {
                target[0] = p0;
                target[1] = p1;
            }
        }

        public struct Payload<T0, T1, T2>
        {
            public T0 p0;
            public T1 p1;
            public T2 p2;

            public void WriteToArray(object[] target)
            {
                target[0] = p0;
                target[1] = p1;
                target[2] = p2;
            }
        }

        public struct Payload<T0, T1, T2, T3>
        {
            public T0 p0;
            public T1 p1;
            public T2 p2;
            public T3 p3;

            public void WriteToArray(object[] target)
            {
                target[0] = p0;
                target[1] = p1;
                target[2] = p2;
                target[3] = p3;
            }
        }
        #endregion

        private const BindingFlags remoteMethodBindingFlags = BindingFlags.NonPublic |
                                                              BindingFlags.Public |
                                                              BindingFlags.Instance |
                                                              BindingFlags.InvokeMethod;

        private struct MethodInvocationContext
        {
            public readonly MethodInfo method;
            public readonly MonoBehaviour component;

            public MethodInvocationContext(MethodInfo method, MonoBehaviour component)
            {
                this.method = method;
                this.component = component;
            }
        }

        internal NetworkObjectID id;
        internal int ownerId;
        internal int prefabIndex;
        public bool hasAuthority { get { return ownerId == Networking.localConnectionId; } }

        private Dictionary<byte, MethodInvocationContext> methodContexts = new Dictionary<byte, MethodInvocationContext>();
        private Dictionary<MethodInfo, byte> methods = new Dictionary<MethodInfo, byte>();

        #region Initialization
        private void Awake()
        {
            GatherRemoteMethods();
        }

        private void GatherRemoteMethods()
        {
            var contextList = new List<MethodInvocationContext>();
            var components = GetComponentsInChildren<MonoBehaviour>();
            foreach (var component in components)
            {
                if (component == this) continue;

                var type = component.GetType();
                do
                {
                    var methodsWithRemoteAttribute = type.GetMethods(remoteMethodBindingFlags).Where(m => m.GetCustomAttributes(typeof(RemoteAttribute), true).Length > 0);
                    foreach (var method in methodsWithRemoteAttribute)
                    {
                        contextList.Add(new MethodInvocationContext(method, component));
                    }
                    type = type.BaseType;
                }
                while (type != typeof(MonoBehaviour));
            }

            var comparer = new CaseInsensitiveComparer();
            contextList.Sort((mA, mB) => comparer.Compare(mA.component.GetType().Name + mA.method.Name, mB.component.GetType().Name + mB.method.Name));

            for (var index = (byte)0; index < contextList.Count; index++)
            {
                methodContexts.Add(index, contextList[index]);
                methods.Add(contextList[index].method, index);
            }
        }
        #endregion

        #region ClientRPC
        private void ClientRPC(MethodInfo method)
        {
            if (methods.TryGetValue(method, out var methodIndex))
            {
                Networking.SendClientRPC(new MessageToObject(id, methodIndex, null));
            }
            else
            {
                throw new UnregisteredRemoteMethodException(method);
            }
        }

        private void ClientRPC<T>(MethodInfo method, T payload)
        {
            if (methods.TryGetValue(method, out var methodIndex))
            {
                Networking.SendClientRPC(new MessageToObject(id, methodIndex, MessageSerializer.Serialize(payload)));
            }
            else
            {
                throw new UnregisteredRemoteMethodException(method);
            }
        }

        /// <summary>
        /// Sends an RPC to all clients. Server only.
        /// </summary>
        /// <param name="method">The method to call on the clients. Must have the [Remote] attribute.</param>
        public void ClientRPC(Action method)
        {
            ClientRPC(method.GetMethodInfo());
        }

        /// <summary>
        /// Sends an RPC to all clients. Server only.
        /// </summary>
        /// <param name="method">The method to call on the clients. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0>(Action<T0> method, T0 p0)
        {
            ClientRPC(method.GetMethodInfo(), p0);
        }

        /// <summary>
        /// Sends an RPC to all clients. Server only.
        /// </summary>
        /// <param name="method">The method to call on the clients. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1>(Action<T0, T1> method, T0 p0, T1 p1)
        {
            ClientRPC(method.GetMethodInfo(), new Payload<T0, T1> { p0 = p0, p1 = p1 });
        }

        /// <summary>
        /// Sends an RPC to all clients. Server only.
        /// </summary>
        /// <param name="method">The method to call on the clients. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1, T2>(Action<T0, T1, T2> method, T0 p0, T1 p1, T2 p2)
        {
            ClientRPC(method.GetMethodInfo(), new Payload<T0, T1, T2> { p0 = p0, p1 = p1, p2 = p2 });
        }

        /// <summary>
        /// Sends an RPC to all clients. Server only.
        /// </summary>
        /// <param name="method">The method to call on the clients. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1, T2, T3>(Action<T0, T1> method, T0 p0, T1 p1, T2 p2, T3 p3)
        {
            ClientRPC(method.GetMethodInfo(), new Payload<T0, T1, T2, T3> { p0 = p0, p1 = p1, p2 = p2, p3 = p3 });
        }

        private void ClientRPC(NetworkClient target, MethodInfo method, object payload)
        {
            if (methods.TryGetValue(method, out var methodIndex))
            {
                var message = new MessageToObject(id, methodIndex, MessageSerializer.Serialize(payload));
                Networking.SendClientRPC(target, message);
            }
            else
            {
                throw new Exception("Trying to call unregistered method remotely.");
            }
        }

        /// <summary>
        /// Sends an RPC to a specific client. Server only.
        /// </summary>
        /// <param name="method">The method to call on the client. Must have the [Remote] attribute.</param>
        public void ClientRPC(NetworkClient target, Action method)
        {
            ClientRPC(target, method.GetMethodInfo(), null);
        }

        /// <summary>
        /// Sends an RPC to a specific client. Server only.
        /// </summary>
        /// <param name="method">The method to call on the client. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0>(NetworkClient target, Action<T0> method, T0 p0)
        {
            ClientRPC(target, method.GetMethodInfo(), p0);
        }

        /// <summary>
        /// Sends an RPC to a specific client. Server only.
        /// </summary>
        /// <param name="method">The method to call on the client. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1>(NetworkClient target, Action<T0, T1> method, T0 p0, T1 p1)
        {
            ClientRPC(target, method.GetMethodInfo(), new Payload<T0, T1> { p0 = p0, p1 = p1 });
        }

        /// <summary>
        /// Sends an RPC to a specific client. Server only.
        /// </summary>
        /// <param name="method">The method to call on the client. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1, T2>(NetworkClient target, Action<T0, T1, T2> method, T0 p0, T1 p1, T2 p2)
        {
            ClientRPC(target, method.GetMethodInfo(), new Payload<T0, T1, T2> { p0 = p0, p1 = p1, p2 = p2 });
        }

        /// <summary>
        /// Sends an RPC to a specific client. Server only.
        /// </summary>
        /// <param name="method">The method to call on the client. Must have the [Remote] attribute.</param>
        public void ClientRPC<T0, T1, T2, T3>(NetworkClient target, Action<T0, T1, T2, T3> method, T0 p0, T1 p1, T2 p2, T3 p3)
        {
            ClientRPC(target, method.GetMethodInfo(), new Payload<T0, T1, T2, T3> { p0 = p0, p1 = p1, p2 = p2, p3 = p3 });
        }
        #endregion

        #region ServerCommand
        private void ServerCommand(MethodInfo method)
        {
            if (methods.TryGetValue(method, out var methodIndex))
            {
                Networking.SendServerCommand(new MessageToObject(id, methodIndex, null));
            }
            else
            {
                throw new UnregisteredRemoteMethodException(method);
            }
        }

        private void ServerCommand<T>(MethodInfo method, T payload)
        {
            if (methods.TryGetValue(method, out var methodIndex))
            {
                Networking.SendServerCommand(new MessageToObject(id, methodIndex, MessageSerializer.Serialize(payload)));
            }
            else
            {
                throw new UnregisteredRemoteMethodException(method);
            }
        }

        /// <summary>
        /// Send an RPC to the server. Client only.
        /// </summary>
        /// <param name="method">The method to call on the server. Must have the [Remote] attribute.</param>
        public void ServerCommand(Action method)
        {
            ServerCommand(method.GetMethodInfo());
        }

        /// <summary>
        /// Send an RPC to the server. Client only.
        /// </summary>
        /// <param name="method">The method to call on the server. Must have the [Remote] attribute.</param>
        public void ServerCommand<T0>(Action<T0> method, T0 p0)
        {
            ServerCommand(method.GetMethodInfo(), p0);
        }

        /// <summary>
        /// Send an RPC to the server. Client only.
        /// </summary>
        /// <param name="method">The method to call on the server. Must have the [Remote] attribute.</param>
        public void ServerCommand<T0, T1>(Action<T0, T1> method, T0 p0, T1 p1)
        {
            ServerCommand(method.GetMethodInfo(), new Payload<T0, T1> { p0 = p0, p1 = p1 });
        }

        /// <summary>
        /// Send an RPC to the server. Client only.
        /// </summary>
        /// <param name="method">The method to call on the server. Must have the [Remote] attribute.</param>
        public void ServerCommand<T0, T1, T2>(Action<T0, T1, T2> method, T0 p0, T1 p1, T2 p2)
        {
            ServerCommand(method.GetMethodInfo(), new Payload<T0, T1, T2> { p0 = p0, p1 = p1, p2 = p2 });
        }

        /// <summary>
        /// Send an RPC to the server. Client only.
        /// </summary>
        /// <param name="method">The method to call on the server. Must have the [Remote] attribute.</param>
        public void ServerCommand<T0, T1, T2, T3>(Action<T0, T1, T2, T3> method, T0 p0, T1 p1, T2 p2, T3 p3)
        {
            ServerCommand(method.GetMethodInfo(), new Payload<T0, T1, T2, T3> { p0 = p0, p1 = p1, p2 = p2, p3 = p3 });
        }
        #endregion

        #region Message Relay
        private static readonly object[] relayParameters1 = new object[1];
        private static readonly object[] relayParameters2 = new object[2];
        private static readonly object[] relayParameters3 = new object[3];
        private static readonly object[] relayParameters4 = new object[4];
        internal void RelayMessage(MessageToObject message)
        {
            if (methodContexts.TryGetValue(message.methodIndex, out var methodContext))
            {
                var methodParameters = methodContext.method.GetParameters();
                var parameters = DeserializePayload(message.payload, methodParameters);

                methodContext.method.Invoke(methodContext.component, parameters);
            }
            else
            {
                Debug.LogError("Could not relay message to object.");
            }
        }

        private static object[] DeserializePayload(byte[] bytes, ParameterInfo[] parameterInfos)
        {
            switch (parameterInfos.Length)
            {
                case 1:
                    relayParameters1[0] = MessageSerializer.Deserialize(bytes, parameterInfos[0].ParameterType);
                    return relayParameters1;
                case 2:
                    using (var stream = new MemoryStream(bytes))
                    {
                        var type = typeof(Payload<,>).MakeGenericType(parameterInfos[0].ParameterType,
                                                                      parameterInfos[1].ParameterType);
                        var payload = MessageSerializer.Deserialize(bytes, type);
                        relayParameters1[0] = relayParameters2;
                        type.GetMethod("WriteToArray").Invoke(payload, relayParameters1);
                        return relayParameters2;
                    }
                case 3:
                    using (var stream = new MemoryStream(bytes))
                    {
                        var type = typeof(Payload<,,>).MakeGenericType(parameterInfos[0].ParameterType,
                                                                       parameterInfos[1].ParameterType,
                                                                       parameterInfos[2].ParameterType);
                        var payload = MessageSerializer.Deserialize(bytes, type);
                        relayParameters1[0] = relayParameters3;
                        type.GetMethod("WriteToArray").Invoke(payload, relayParameters1);
                        return relayParameters3;
                    }
                case 4:
                    using (var stream = new MemoryStream(bytes))
                    {
                        var type = typeof(Payload<,,,>).MakeGenericType(parameterInfos[0].ParameterType,
                                                                        parameterInfos[1].ParameterType,
                                                                        parameterInfos[2].ParameterType,
                                                                        parameterInfos[3].ParameterType);
                        var payload = MessageSerializer.Deserialize(bytes, type);
                        relayParameters1[0] = relayParameters4;
                        type.GetMethod("WriteToArray").Invoke(payload, relayParameters1);
                        return relayParameters4;
                    }
                default:
                    return null;
            }
        }
    }
    #endregion
}
