﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;

    internal class NetworkingWorker : MonoBehaviour
    {
        private void Update()
        {
            if (Networking.worker != this)
            {
                Destroy(gameObject);
                return;
            }
            Networking.Update();
            NetworkTime.Update();
        }

        private void OnApplicationQuit()
        {
            Networking.Disconnect();
        }
    }
}
