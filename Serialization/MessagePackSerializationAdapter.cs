﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using System.IO;
    using MessagePack;

    [CreateAssetMenu(menuName = "Batworking/Serialization Adapter/MessagePack", fileName = "Serialization Adapter - MessagePack")]
    public class MessagePackSerializationAdapter : SerializationAdapterBase
    {
        private static IFormatterResolver resolver => MessagePack.Resolvers.ContractlessStandardResolver.Instance;

        public override void Serialize<T>(Stream stream, T obj)
        {
            MessagePackSerializer.Serialize(stream, obj, resolver);
        }

        public override byte[] Serialize<T>(T obj)
        {
            return MessagePackSerializer.Serialize(obj, resolver);
        }

        public override T Deserialize<T>(Stream stream)
        {
            return MessagePackSerializer.Deserialize<T>(stream, resolver);
        }

        public override T Deserialize<T>(byte[] bytes)
        {
            return MessagePackSerializer.Deserialize<T>(bytes, resolver);
        }
    }
}
