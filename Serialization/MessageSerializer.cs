﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using System;
    using System.IO;
    using System.Collections.Generic;
    using System.Reflection;

    internal static class MessageSerializer
    {
        private static BijectiveDictionary<byte, Type> messageTypes;

        private static readonly List<byte> buffer = new List<byte>();
        private static SerializationAdapterBase serializationAdapter;

        private static readonly MethodInfo genericDeserializeMessageMethod;
        private static readonly MethodInfo genericDeserializeMethod;
        private static readonly object[] methodInvocationParameters = new object[1];
        
        
        static MessageSerializer()
        {
            genericDeserializeMessageMethod = typeof(MessageSerializer).GetMethod("DeserializeMessage", BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.InvokeMethod, null, new Type[] { typeof(byte[]) }, null);
            genericDeserializeMethod = typeof(MessageSerializer).GetMethod("Deserialize", new Type[] { typeof(byte[]) });
        }

        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            CreateMessageTypeDictionary();
            serializationAdapter = BatworkingSetup.activeSetup.serializationAdapter;
        }

        #region Create MessageType Dictionary
        private static void CreateMessageTypeDictionary()
        {
            messageTypes = new BijectiveDictionary<byte, Type>();

            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    var messageAttribute = GetAttribute<NetworkMessageAttribute>(type);
                    if (messageAttribute != null)
                    {
                        var code = messageAttribute.code;

                        if (messageTypes.TryGetValue(code, out var otherType))
                        {
                            Debug.LogError("Adding multiple message types with code " + code + ": " + otherType.Name + " / " + type.Name);
                        }
                        else
                        {
                            messageTypes.TryAdd(code, type);
                        }
                    }
                }
            }
        }

        private static T GetAttribute<T>(Type type) where T : Attribute
        {
            var attributes = type.GetCustomAttributes(typeof(T), false);
            if (attributes.Length > 0)
            {
                return (T)attributes[0];
            }
            return null;
        }
        #endregion

        #region Message Serialization
        internal static byte[] SerializeMessage<T>(T message)
        {
            if (!messageTypes.TryGetKey(typeof(T), out var code))
            {
                return null;
            }

            buffer.Add(code);
            
            // TODO Check if a static stream could be reused
            // TODO Check if this works without the buffer list
            using (var stream = new MemoryStream())
            {
                serializationAdapter.Serialize(stream, message);
                foreach (var b in stream.ToArray())
                {
                    buffer.Add(b);
                }
            }

            var result = buffer.ToArray();
            buffer.Clear();
            return result;
        }
        #endregion

        #region Serialization
        public static byte[] Serialize<T>(T obj)
        {
            return serializationAdapter.Serialize(obj);
        }
        #endregion

        #region Message Deserialization
        internal static object ConvertToMessage(byte[] bytes)
        {
            object message = null;

            var code = bytes[0];
            if (messageTypes.TryGetValue(code, out var messageType))
            {
                message = DeserializeMessage(bytes, messageType);
            }
            return message;
        }


        private static object DeserializeMessage(byte[] bytes, Type type)
        {
            var methodInfo = genericDeserializeMessageMethod.MakeGenericMethod(type);
            methodInvocationParameters[0] = bytes;
            var result = methodInfo.Invoke(null, methodInvocationParameters);
            return result;
        }

        private static T DeserializeMessage<T>(byte[] bytes)
        {
            // TODO Check if a static stream can be reused
            using (var stream = new MemoryStream(bytes, 1, bytes.Length - 1))
            {
                return serializationAdapter.Deserialize<T>(stream);
            }
        }
        #endregion

        #region Deserialization
        public static T Deserialize<T>(byte[] bytes)
        {
            return serializationAdapter.Deserialize<T>(bytes);
        }

        public static object Deserialize(byte[] bytes, Type type)
        {
            var methodInfo = genericDeserializeMethod.MakeGenericMethod(type);
            methodInvocationParameters[0] = bytes;
            var result = methodInfo.Invoke(null, methodInvocationParameters);
            return result;
        }
        #endregion
    }
}
