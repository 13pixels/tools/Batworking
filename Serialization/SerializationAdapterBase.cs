﻿
namespace ThirteenPixels.Batworking
{
    using System.IO;
    using UnityEngine;
    
    public abstract class SerializationAdapterBase : ScriptableObject
    {
        public abstract void Serialize<T>(Stream stream, T obj);
        public abstract byte[] Serialize<T>(T obj);
        public abstract T Deserialize<T>(Stream stream);
        public abstract T Deserialize<T>(byte[] bytes);
    }
}