﻿
namespace ThirteenPixels.Batworking
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public static class Networking
    {
        private static TransportLayerBase transportLayer;
        public static bool isClient => transportLayer != null && transportLayer.isClient;
        public static bool isServer => transportLayer != null && transportLayer.isServer;

        internal static NetworkingWorker worker { get; private set; }

        private static List<int> serverConnections = new List<int>();
        internal static int localConnectionId { get; private set; } = -1;

        private static Dictionary<NetworkObjectID, NetworkObject> networkObjects = new Dictionary<NetworkObjectID, NetworkObject>();
        private static Dictionary<int, List<NetworkObjectID>> networkObjectsByOwner = new Dictionary<int, List<NetworkObjectID>>();
        private static NetworkObjectID nextId = 0;

        #region Initialization
        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            if (TryInitializeTransportLayer())
            {
                CreateWorker();
            }
        }

        private static bool TryInitializeTransportLayer()
        {
            transportLayer = BatworkingSetup.activeSetup.transportLayer;

            if (transportLayer != null)
            {
                SetTransportLayerLoggers();
                return true;
            }
            else
            {
                Debug.LogError("Could not initialize transport layer.");
                return false;
            }
        }

        private static void SetTransportLayerLoggers()
        {
            transportLayer.logger.log = Debug.Log;
            transportLayer.logger.logWarning = Debug.LogWarning;
            transportLayer.logger.logError = Debug.LogError;
        }

        private static void CreateWorker()
        {
            var go = new GameObject("Telepathy Network Worker");
            go.hideFlags = HideFlags.HideAndDontSave;
            worker = go.AddComponent<NetworkingWorker>();
        }
        #endregion

        internal static void Update()
        {
            transportLayer.Update();
        }

        #region Internal OnConnection Methods
        internal static void OnClientConnectedToServer(int connectionId)
        {
            serverConnections.Add(connectionId);
            networkObjectsByOwner.Add(connectionId, new List<NetworkObjectID>());

            // Inform the new client about its id
            SendClientRPC(connectionId, new MessageOnConnect(connectionId, Time.time));

            SendAllNetworkedObjectInstancesTo(connectionId);

            ServerSpawnPlayerObject(connectionId);
        }

        internal static void OnClientDisconnected()
        {
            if (!isServer)
            {
                DestroyAllNetworkObjects();
            }
            localConnectionId = -1;
        }

        internal static void OnClientDisconnectedFromServer(int connectionId)
        {
            serverConnections.Remove(connectionId);
            DestroyAllNetworkObjectsOwnedBy(connectionId);
            SendClientRPC(new MessageOnDisconnect(connectionId));
        }
        #endregion

        #region Message Processing
        internal static void ProcessClientMessage(object message)
        {
            var type = message.GetType();
            
            if (type == typeof(MessageOnConnect))
            {
                var msg = (MessageOnConnect)message;
                localConnectionId = msg.connectionId;
                networkObjectsByOwner.Add(msg.connectionId, new List<NetworkObjectID>());
                NetworkTime.time = msg.serverTime;
            }
            else if (type == typeof(MessageSpawnMass))
            {
                var msg = (MessageSpawnMass)message;
                SpawnLocally(msg);
            }
            else if (type == typeof(MessageSpawn))
            {
                var msg = (MessageSpawn)message;
                SpawnLocally(msg);
            }
            else if (type == typeof(MessageToObject))
            {
                var msg = (MessageToObject)message;
                HandleMessageToObject(msg);
            }
            else if (type == typeof(MessageOnDisconnect))
            {
                var msg = (MessageOnDisconnect)message;
                DestroyAllNetworkObjectsOwnedBy(msg.connectionId);
            }
            else
            {
                Debug.LogError("Invalid message type.");
            }
        }

        internal static void ProcessServerMessage(object message)
        {
            var type = message.GetType();

            if (type == typeof(MessageToObject))
            {
                var msg = (MessageToObject)message;
                HandleMessageToObject(msg);
            }
            else
            {
                Debug.LogError("Invalid message type.");
            }
        }

        private static void HandleMessageToObject(MessageToObject msg)
        {
            if (networkObjects.TryGetValue(msg.target, out var obj))
            {
                obj.RelayMessage(msg);
            }
            else
            {
                Debug.LogError("Received message to unknown object.");
            }
        }
        #endregion

        #region RPCs and Commands
        /// <summary>
        /// Sends the provided message to a specific client. Server only.
        /// </summary>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <param name="connectionID">The connectionID of the client to sent the message to.</param>
        /// <param name="message">The message to send.</param>
        public static void SendClientRPC<T>(int connectionID, T message)
        {
            if (isServer)
            {
                var bytes = MessageSerializer.SerializeMessage(message);
                if (bytes != null)
                {
                    transportLayer.SendClientRPC(connectionID, bytes);
                }
                else
                {
                    Debug.LogError("Trying to send a message that is not marked with [NetworkMessage].");
                }
            }
            else
            {
                Debug.LogError("Trying to send a client RPC while the server is not running.");
            }
        }

        /// <summary>
        /// Sends the provided message to all connected clients. Server only.
        /// </summary>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <param name="message">The message to send.</param>
        public static void SendClientRPC<T>(T message)
        {
            if (isServer)
            {
                var bytes = MessageSerializer.SerializeMessage(message);
                if (bytes != null)
                {
                    foreach (var connectionID in serverConnections)
                    {
                        transportLayer.SendClientRPC(connectionID, bytes);
                    }
                }
                else
                {
                    Debug.LogError("Trying to send a message that is not marked with [NetworkMessage].");
                }
            }
            else
            {
                Debug.LogError("Trying to send a client RPC while the server is not running.");
            }
        }

        /// <summary>
        /// Sends the provided message to the server. Client only.
        /// </summary>
        /// <typeparam name="T">The type of the message.</typeparam>
        /// <param name="message">The message to send.</param>
        public static void SendServerCommand<T>(T message)
        {
            if (isClient)
            {
                var bytes = MessageSerializer.SerializeMessage(message);
                if (bytes != null)
                {
                    transportLayer.SendServerCommand(bytes);
                }
                else
                {
                    Debug.LogError("Trying to send a message that is not marked with [NetworkMessage].");
                }
            }
            else
            {
                Debug.LogError("Trying to send a server command while not connected to a server.");
            }
        }
        #endregion

        #region Connect and Disconnect
        public static void StartServer(int port)
        {
            if (isClient) return;

            transportLayer.StartServer(port);
        }

        public static void ConnectClient(string ip, int port)
        {
            // TODO When isServer, maybe don't connect, but do local stuff?
            transportLayer.ConnectClient(ip, port);
        }

        public static void Disconnect()
        {
            DestroyAllNetworkObjects();
            transportLayer.Disconnect();
        }
        #endregion

        #region Spawning
        private static NetworkObject ServerSpawnPlayerObject(int connectionId)
        {
            return ServerSpawn(0, connectionId);
        }
        
        private static NetworkObject ServerSpawn(int prefabIndex, int ownerId)
        {
            var instance = SpawnLocally(prefabIndex, nextId, ownerId);
            SendClientRPC(new MessageSpawn(ownerId, prefabIndex, nextId));

            nextId++;

            return instance;
        }

        private static NetworkObject SpawnLocally(MessageSpawn message)
        {
            return SpawnLocally(message.prefabIndex, message.networkObjectId, message.ownerId);
        }

        private static void SpawnLocally(MessageSpawnMass message)
        {
            foreach (var item in message.spawns)
            {
                SpawnLocally(item.prefabIndex, item.networkObjectId, item.ownerId);
                // TODO Initialization logic?
            }
        }

        private static NetworkObject SpawnLocally(int prefabIndex, NetworkObjectID id, int ownerId)
        {
            var prefab = NetworkSceneSetup.singleton.GetPrefab(prefabIndex);
            if (prefab == null) throw new Exception("Trying to spawn an unknown prefab.");

            var instance = UnityEngine.Object.Instantiate(prefab);

            instance.id = id;
            instance.ownerId = ownerId;
            instance.prefabIndex = prefabIndex;

            networkObjects.Add(id, instance);
            if (!networkObjectsByOwner.ContainsKey(ownerId))
            {
                networkObjectsByOwner.Add(ownerId, new List<NetworkObjectID>());
            }
            networkObjectsByOwner[ownerId].Add(id);

            return instance;
        }

        private static void SendAllNetworkedObjectInstancesTo(int connectionId)
        {
            var count = networkObjects.Count;
            if (count == 0) return;

            var spawns = new MessageSpawn[count];
            var i = 0;
            foreach (var obj in networkObjects.Values)
            {
                spawns[i] = new MessageSpawn(obj.ownerId, obj.prefabIndex, obj.id);
                i++;
            }
            var message = new MessageSpawnMass(spawns);
            SendClientRPC(connectionId, message);
        }

        private static void DestroyAllNetworkObjects()
        {
            foreach (var obj in networkObjects.Values)
            {
                UnityEngine.Object.Destroy(obj.gameObject);
            }
            networkObjects.Clear();
            networkObjectsByOwner.Clear();
        }

        private static void DestroyAllNetworkObjectsOwnedBy(int connectionId)
        {
            foreach (var id in networkObjectsByOwner[connectionId])
            {
                var obj = networkObjects[id];
                UnityEngine.Object.Destroy(obj.gameObject);
                networkObjects.Remove(id);
            }
            networkObjectsByOwner.Remove(connectionId);
        }
        #endregion
    }
}
