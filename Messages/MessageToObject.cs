﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable, NetworkMessage(4)]
    public struct MessageToObject
    {
        public readonly NetworkObjectID target;
        public readonly byte methodIndex;
        public readonly byte[] payload;

        public MessageToObject(NetworkObjectID target, byte methodIndex, byte[] payload)
        {
            this.target = target;
            this.methodIndex = methodIndex;
            this.payload = payload;
        }
    }

    [System.Serializable, NetworkMessage(5)]
    public struct MessageToObject<T>
    {
        public readonly NetworkObjectID target;
        public readonly byte methodIndex;
        public readonly T payload;

        public MessageToObject(NetworkObjectID target, byte methodIndex, T payload)
        {
            this.target = target;
            this.methodIndex = methodIndex;
            this.payload = payload;
        }
    }
}