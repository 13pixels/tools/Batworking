﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable, NetworkMessage(1)]
    public struct MessageOnConnect
    {
        public readonly int connectionId;
        public readonly float serverTime;

        public MessageOnConnect(int connectionId, float serverTime)
        {
            this.connectionId = connectionId;
            this.serverTime = serverTime;
        }
    }
}