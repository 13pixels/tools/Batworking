﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable, NetworkMessage(3)]
    public struct MessageSpawnMass
    {
        public readonly MessageSpawn[] spawns;

        public MessageSpawnMass(MessageSpawn[] spawns)
        {
            this.spawns = spawns;
        }
    }
}