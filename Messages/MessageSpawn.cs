﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable, NetworkMessage(2)]
    public struct MessageSpawn
    {
        public readonly int ownerId;
        public readonly int prefabIndex;
        public readonly NetworkObjectID networkObjectId;

        public MessageSpawn(int ownerId, int prefabIndex, NetworkObjectID networkObjectId)
        {
            this.ownerId = ownerId;
            this.prefabIndex = prefabIndex;
            this.networkObjectId = networkObjectId;
        }
    }
}