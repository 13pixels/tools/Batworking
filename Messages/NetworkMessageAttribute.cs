﻿
namespace ThirteenPixels.Batworking
{
    using System;

    [AttributeUsage(AttributeTargets.Struct)]
    public class NetworkMessageAttribute : Attribute
    {
        internal byte code;

        public NetworkMessageAttribute(byte code)
        {
            this.code = code;
        }
    }
}
