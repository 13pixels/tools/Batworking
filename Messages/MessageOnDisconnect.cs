﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable, NetworkMessage(9)]
    public struct MessageOnDisconnect
    {
        public readonly int connectionId;

        public MessageOnDisconnect(int connectionId)
        {
            this.connectionId = connectionId;
        }
    }
}