**This software is in a very early state. Code needs to be cleaned up, improved, extended and documented. Use at your own risk.**

Modular, lightweight high-level networking for Unity.

# Setup
* Clone this repository into your Assets.
* It doesn't come with any modules. Pick one from each list below and import them as well; or implement your own.

## Currently supported modules
### Serialization
* [MessagePack](https://github.com/neuecc/MessagePack-CSharp) (get the UnityPackage from [here](https://github.com/neuecc/MessagePack-CSharp/releases))

### Network Transport
* [Telepathy](https://github.com/vis2k/Telepathy)

## Project and Scene Setup
The following steps are currently already done after downloading Batworking:
* Make sure a `BatworkingSetup` ScriptableObject exists in Resources Folder with the path (`Batworking/Setup`).
* Create all ScriptableObjects needed to fill the Setup object, and fill it.

Follow these steps to set up your first networking scene:
* In your scene, add a `NetworkSetup` component to a GameObject.
* For quick debugging, also add a `NetworkDebugUI` component.
* Create a prefab for the players.
  * Add a `NetworkObject` component.
  * Add a script that sends data; examples can be found in the repository's snippets.
* Drag the player prefab into the `Player Prefab` property of the `NetworkSetup` component.
