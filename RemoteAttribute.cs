﻿
namespace ThirteenPixels.Batworking
{
    using System;

    /// <summary>
    /// Marks a method as being callable remotely.
    /// The method needs to be passed to the NetworkObject component on the same GameObject or a parent.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class RemoteAttribute : Attribute
    {

    }
}
