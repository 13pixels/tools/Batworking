﻿
namespace ThirteenPixels.Batworking
{
    using System;

    public struct TransportLayerLogger
    {
        public Action<object> log;
        public Action<object> logWarning;
        public Action<object> logError;
    }
}