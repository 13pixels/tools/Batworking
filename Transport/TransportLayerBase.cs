﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;

    public abstract class TransportLayerBase : ScriptableObject
    {
        public abstract bool isServer { get; }
        public abstract bool isClient { get; }

        public TransportLayerLogger logger = new TransportLayerLogger();


        public abstract void StartServer(int port);
        public abstract void ConnectClient(string ip, int port);
        // TODO Would it make sense to disconnect client and server seperately in non-dedicated host cases?
        public abstract void Disconnect();

        public abstract void Update();

        // TODO Check if this would work without arrays, but IEnumerable or streams
        public abstract void SendClientRPC(int connectionID, byte[] bytes);
        public abstract void SendServerCommand(byte[] bytes);
    }
}
