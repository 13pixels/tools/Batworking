﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using Telepathy;

    [CreateAssetMenu(menuName = "Batworking/Transport Layer/Telepathy", fileName = "Transport Layer - Telepathy")]
    public class TransportLayerTelepathy : TransportLayerBase
    {
        private Client client = new Client();
        private Server server = new Server();
        public override bool isServer => server.Active;
        public override bool isClient => client.Connected;
        private bool wasClient = false;

        [SerializeField]
        private bool noDelay = default;


        #region Connection
        public override void StartServer(int port)
        {
            server.NoDelay = noDelay;
            server.Start(port);
        }

        public override void ConnectClient(string ip, int port)
        {
            client.NoDelay = noDelay;
            client.Connect(ip, port);
        }

        public override void Disconnect()
        {
            server.Stop();
            client.Disconnect();
            wasClient = false;
        }
        #endregion

        public override void Update()
        {
            if (isClient)
            {
                ProcessClientMessages();
                wasClient = true;
            }
            else if (wasClient)
            {
                OnLoseConnectionToServer();
            }

            if (isServer)
            {
                ProcessServerMessages();
            }
        }

        private static void OnLoseConnectionToServer()
        {
            // TODO Consider adding a timeout or some way to handle this state
            Networking.OnClientDisconnected();
        }

        #region Message Processing
        private void ProcessClientMessages()
        {
            while (client.GetNextMessage(out var telepathyMessage))
            {
                switch (telepathyMessage.eventType)
                {
                    case Telepathy.EventType.Connected:
                        // Debug.Log("Connected");
                        break;

                    case Telepathy.EventType.Data:
                        var message = MessageSerializer.ConvertToMessage(telepathyMessage.data);
                        if (message != null)
                        {
                            Networking.ProcessClientMessage(message);
                        }
                        else
                        {
                            logger.logError?.Invoke("Received unknown event.");
                        }
                        break;

                    case Telepathy.EventType.Disconnected:
                        // This doesn't seem to ever happen
                        break;
                }
            }
        }

        private void ProcessServerMessages()
        {
            while (server.GetNextMessage(out var telepathyMessage))
            {
                switch (telepathyMessage.eventType)
                {
                    case Telepathy.EventType.Connected:
                        Networking.OnClientConnectedToServer(telepathyMessage.connectionId);
                        logger.log?.Invoke(telepathyMessage.connectionId + " Connected");
                        break;

                    case Telepathy.EventType.Data:
                        var message = MessageSerializer.ConvertToMessage(telepathyMessage.data);
                        if (message != null)
                        {
                            Networking.ProcessServerMessage(message);
                        }
                        else
                        {
                            logger.logError?.Invoke("Received unknown event.");
                        }
                        break;

                    case Telepathy.EventType.Disconnected:
                        Networking.OnClientDisconnectedFromServer(telepathyMessage.connectionId);
                        logger.log?.Invoke(telepathyMessage.connectionId + " Disconnected");
                        break;
                }
            }
        }
        #endregion

        #region Message Sending
        public override void SendServerCommand(byte[] bytes)
        {
            client.Send(bytes);
        }

        public override void SendClientRPC(int connectionID, byte[] bytes)
        {
            server.Send(connectionID, bytes);
        }
        #endregion
    }
}