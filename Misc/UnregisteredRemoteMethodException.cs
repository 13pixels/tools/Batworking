﻿
namespace ThirteenPixels.Batworking
{
    using System;
    using System.Reflection;

    public class UnregisteredRemoteMethodException : Exception
    {
        public UnregisteredRemoteMethodException(MethodInfo methodInfo) : base("Trying to call unregistered method remotely: " + methodInfo.Name)
        {
            
        }
    }
}