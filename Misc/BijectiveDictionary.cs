﻿
namespace ThirteenPixels.Batworking
{
    using System.Collections.Generic;

    public class BijectiveDictionary<TKey, TValue>
    {
        private Dictionary<TKey, TValue> regularDict;
        private Dictionary<TValue, TKey> reverseDict;

        public ICollection<TKey> Keys => regularDict.Keys;
        public ICollection<TValue> Values => reverseDict.Keys;
        public int Count => regularDict.Count;

        public BijectiveDictionary()
        {
            regularDict = new Dictionary<TKey, TValue>();
            reverseDict = new Dictionary<TValue, TKey>();
        }

        public bool TryAdd(TKey a, TValue b)
        {
            if (!regularDict.ContainsKey(a) && !reverseDict.ContainsKey(b))
            {
                regularDict.Add(a, b);
                reverseDict.Add(b, a);
                return true;
            }
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return regularDict.TryGetValue(key, out value);
        }

        public bool TryGetKey(TValue value, out TKey key)
        {
            return reverseDict.TryGetValue(value, out key);
        }

        public bool Remove(TKey key)
        {
            if (regularDict.TryGetValue(key, out var value))
            {
                regularDict.Remove(key);
                reverseDict.Remove(value);
                return true;
            }
            return false;
        }

        public bool RemoveValue(TValue value)
        {
            if (reverseDict.TryGetValue(value, out var key))
            {
                regularDict.Remove(key);
                reverseDict.Remove(value);
                return true;
            }
            return false;
        }

        public bool ContainsKey(TKey key)
        {
            return regularDict.ContainsKey(key);
        }

        public bool ContainsValue(TValue value)
        {
            return reverseDict.ContainsKey(value);
        }

        public void Clear()
        {
            regularDict.Clear();
            reverseDict.Clear();
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return regularDict.GetEnumerator();
        }
    }
}
