﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;

    public static class NetworkTime
    {
        // TODO Consider testing for syncs in specified intervals
        private static float _time;
        /// <summary>
        /// The synced time. Should be more or less the same on server and all clients at any given time.
        /// </summary>
        public static float time
        {
            get
            {
                return Networking.isServer ? Time.time : _time;
            }
            internal set
            {
                _time = value;
            }
        }

        internal static void Update()
        {
            _time += Time.deltaTime;
        }
    }
}