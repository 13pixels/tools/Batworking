﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;

    [CreateAssetMenu(menuName = "Batworking/Setup", fileName = "Batworking Setup")]
    public class BatworkingSetup : ScriptableObject
    {
        private const string resourcesPath = "Batworking/Batworking Setup";

        private static BatworkingSetup _activeSetup;
        internal static BatworkingSetup activeSetup
        {
            get
            {
                if (!_activeSetup)
                {
                    _activeSetup = Resources.Load<BatworkingSetup>(resourcesPath);
                }
                return _activeSetup;
            }
        }

        [SerializeField]
        private SerializationAdapterBase _serializationAdapter = default;
        internal SerializationAdapterBase serializationAdapter {  get { return _serializationAdapter; } }

        [SerializeField]
        private TransportLayerBase _transportLayer = default;
        internal TransportLayerBase transportLayer { get { return _transportLayer; } }
    }
}