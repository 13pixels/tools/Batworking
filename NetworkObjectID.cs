﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable]
    public struct NetworkObjectID
    {
        public readonly int id;

        public NetworkObjectID(int id)
        {
            this.id = id;
        }

        public static implicit operator NetworkObjectID(int id)
        {
            return new NetworkObjectID(id);
        }

        public static implicit operator int(NetworkObjectID id)
        {
            return id.id;
        }
    }
}
