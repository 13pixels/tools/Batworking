﻿
namespace ThirteenPixels.Batworking
{
    [System.Serializable]
    public struct NetworkClient
    {
        public readonly int id;

        public NetworkClient(int id)
        {
            this.id = id;
        }

        public static implicit operator int(NetworkClient client)
        {
            return client.id;
        }
    }
}