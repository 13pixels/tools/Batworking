﻿using UnityEngine;
using ThirteenPixels.Batworking;

public class NetworkDebugUI : MonoBehaviour
{
    private string ip = "localhost";
    private int port = 777;

    private void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 200, 100));

        GUILayout.BeginHorizontal();
        ip = GUILayout.TextField(ip);
        int.TryParse(GUILayout.TextField(port + "", 4), out port);
        GUILayout.EndHorizontal();

        var isOnline = Networking.isClient || Networking.isServer;

        if (Button("Start Server", !isOnline))
        {
            Networking.StartServer(port);
        }
        if (Button("Connect Client", !Networking.isClient))
        {
            Networking.ConnectClient(ip, port);
        }
        if (Button("Disconnect", isOnline))
        {
            Networking.Disconnect();
        }

        GUILayout.EndArea();
    }

    private bool Button(string text, bool condition = true)
    {
        if (condition)
        {
            return GUILayout.Button(text);
        }
        GUILayout.Box(text);
        return false;
    }
}
