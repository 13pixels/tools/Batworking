﻿// #define RECALCULATE_HISTORY

namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using System.Collections.Generic;

    [RequireComponent(typeof(NetworkObject))]
    public abstract class NetworkSimulation<TInput, TResult> : MonoBehaviour
    {
        protected virtual float authorityClientSyncInterval { get { return 0.4f; } }
        // TODO implement
        private const float otherClientSyncInterval = 0.1f;
        // TODO implement
        private const float inputCommandInterval = 0.1f;

        [System.Serializable]
        public struct SimulationStep
        {
            public double time;
            public TInput input;
            // TODO Really send the result to the server?
            public TResult result;

            public SimulationStep(TInput input, TResult result)
            {
                time = NetworkTime.time;
                this.input = input;
                this.result = result;
            }
        }

        [System.Serializable]
        public struct ServerSimulationStep
        {
            public double time;
            public TResult result;

            public ServerSimulationStep(double time, TResult result)
            {
                this.time = time;
                this.result = result;
            }
        }

        private NetworkObject networkObject;

        private Queue<SimulationStep> simulationHistory = new Queue<SimulationStep>();
        private bool hasSimulationHistory => simulationHistory.Count > 0;
        private float timeSinceLastResultSync = 0f;

        protected abstract TResult SimulateStep(TInput input);
        protected abstract TInput GetLocalInput();
        protected abstract TInput GetValidatedInput(TInput input);
        protected abstract void ApplySimulationResult(TResult result);


        protected virtual void Awake()
        {
            networkObject = GetComponent<NetworkObject>();
        }

        protected virtual void FixedUpdate()
        {
            if (networkObject.hasAuthority)
            {
                ClientSimulate();
            }
            else if (Networking.isServer)
            {
                ServerHandleSimulationQueue();
            }
        }

        // [Client]
        private void ClientSimulate()
        {
            var input = GetLocalInput();
            var result = SimulateStep(input);
            var step = new SimulationStep(input, result);
            if (Networking.isServer)
            {
                networkObject.ClientRPC(RpcApplySimulationResult, result);
            }
            else
            {
                simulationHistory.Enqueue(step);
                networkObject.ServerCommand(CmdClientSimulated, step);
            }
        }

        // [Command]
        [Remote]
        private void CmdClientSimulated(SimulationStep clientStep)
        {
            simulationHistory.Enqueue(clientStep);
        }

        // [Server]
        private void ServerHandleSimulationQueue()
        {
            var simulationTime = NetworkTime.time;
            // TODO Check if the updates really happened in the proper interval
            while (hasSimulationHistory && simulationHistory.Peek().time < simulationTime)
            {
                var clientStep = simulationHistory.Dequeue();
                var serverResult = SimulateStep(GetValidatedInput(clientStep.input));

                // if (!Equals(clientStep.result, serverResult))
                // {
                //     onPredicionError.Invoke();
                // }

                SyncResultsOnInterval(clientStep, serverResult);
            }
        }

        // [Server]
        private void SyncResultsOnInterval(SimulationStep clientStep, TResult serverResult)
        {
            var syncResults = false;
            if (authorityClientSyncInterval > 0)
            {
                timeSinceLastResultSync += Time.deltaTime;
                if (timeSinceLastResultSync >= authorityClientSyncInterval)
                {
                    timeSinceLastResultSync -= authorityClientSyncInterval;
                    syncResults = true;
                }
            }

            if (syncResults)
            {
                networkObject.ClientRPC(RpcClientHandleSimulationStep, new ServerSimulationStep(clientStep.time, serverResult));
            }
        }

        // [ClientRpc]
        [Remote]
        private void RpcClientHandleSimulationStep(ServerSimulationStep serverStep)
        {
            if (networkObject.hasAuthority)
            {
                SimulationStep clientStep;
                do
                {
                    clientStep = simulationHistory.Dequeue();
                }
                while (clientStep.time < serverStep.time);

                // Assert.IsTrue(clientStep.time == serverStep.time);

                if (!Equals(clientStep.result, serverStep.result))
                {
                    // onPredicionError.Invoke();
#if RECALCULATE_HISTORY
                    RecalculateHistory(serverStep.result);
#else
                    ApplySimulationResult(serverStep.result);
#endif
                }
            }
            else
            {
                ApplySimulationResult(serverStep.result);
            }
        }

        // [ClientRpc]
        [Remote]
        private void RpcApplySimulationResult(TResult result)
        {
            ApplySimulationResult(result);
        }

        // [Client]
        private void RecalculateHistory(TResult latestResult)
        {
            ApplySimulationResult(latestResult);

            var stepCount = simulationHistory.Count;
            for (var i = 0; i < stepCount; i++)
            {
                var step = simulationHistory.Dequeue();
                step.result = SimulateStep(step.input);
                simulationHistory.Enqueue(step);
            }
        }
    }
}