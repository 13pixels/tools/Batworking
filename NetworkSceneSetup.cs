﻿
namespace ThirteenPixels.Batworking
{
    using UnityEngine;
    using System.Collections.Generic;

    public class NetworkSceneSetup : MonoBehaviour
    {
        internal static NetworkSceneSetup singleton { get; private set; }

        [SerializeField]
        private NetworkObject _playerPrefab = default;
        internal NetworkObject playerPrefab { get { return _playerPrefab; } }

        [SerializeField]
        private List<NetworkObject> spawnablePrefabs = default;

        private void Awake()
        {
            if (singleton)
            {
                Destroy(this);
            }
            else
            {
                singleton = this;
            }
        }

        private void OnDestroy()
        {
            if (singleton == this)
            {
                singleton = null;
            }
        }

        internal NetworkObject GetPrefab(int index)
        {
            if (index < 0) return null;
            if (index == 0)
            {
                return playerPrefab;
            }
            if (index <= spawnablePrefabs.Count)
            {
                return spawnablePrefabs[index - 1];
            }
            return null;
        }

        internal int GetPrefabIndex(NetworkObject prefab)
        {
            if (prefab == playerPrefab)
            {
                return 0;
            }
            else
            {
                var index = spawnablePrefabs.IndexOf(prefab);
                if (index >= 0)
                {
                    return index + 1;
                }
                return -1;
            }
        }
    }
}
